import { showModal } from './modal';
//import App from '../../../App';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
    // call showModal function
    const winnerInfo = {
        title: `Winner: ${fighter.name}`,
        bodyElement: createFighterImage(fighter),
        onClose: restart,
    };
    showModal(winnerInfo);
}

function restart() {
    //  App.rootElement.innerHTML = '';
    //  new App();
    window.location.reload();
}
