import { controls } from '../../constants/controls';

const critTimeout = 10000;
const duel = getNewDuel();

export async function fight(firstFighter, secondFighter) {
    document.addEventListener('keydown', duel.keyDown);
    document.addEventListener('keyup', duel.keyUp);
    duel.startNew(firstFighter, secondFighter);
    return new Promise(resolve => {
        // resolve the promise with the winner when fight is over
        duel.resolve = resolve;
    });
}

export function getDamage(attacker, defender) {
    // return damage
    return Math.max(0, getHitPower(attacker) - getBlockPower(defender));
}

export function getHitPower(fighter) {
    // return hit power
    return fighter.attack * (1 + Math.random());
}

export function getBlockPower(fighter) {
    // return block power
    return fighter.defense * (1 + Math.random());
}

function getNewDuel() {
    const duel = new Map();
    duel.pressedKeys = new Set();

    duel.startNew = function (firstFighter, secondFighter) {
        this.clear();
        this.set(firstFighter, getFirstPlayer(firstFighter.health, this.pressedKeys));
        this.set(secondFighter, getSecondPlayer(secondFighter.health, this.pressedKeys));
    };

    // binded to this duel object
    duel.keyDown = function (e) {
        if (!duel.pressedKeys.has(e.code)) {
            duel.pressedKeys.add(e.code);
            duel.proceed();
        }
    };

    // binded to this duel object
    duel.keyUp = function (e) {
        duel.pressedKeys.delete(e.code);
    };

    duel.proceed = function () {
        let fighters = [...this.keys()];
        this.check(fighters[0], fighters[1]);
        this.check(fighters[1], fighters[0]);
    };

    duel.check = function (attacker, defender) {
        if (this.kick(attacker, defender)) {
            if (this.get(defender).terminated()) {
                document.removeEventListener('keydown', duel.keyDown);
                document.removeEventListener('keyup', duel.keyUp);
                duel.resolve(attacker);
            }
        }
    };

    duel.kick = function (attacker, defender) {
        if (this.get(attacker).action.isCrit()) {
            this.get(defender).damaged(attacker.attack * 2);
            return true;
        }
        if (this.get(attacker).action.isAttack() && !this.get(defender).action.isBlock()) {
            this.get(defender).damaged(getDamage(attacker, defender));
            return true;
        }
        return false;
    };

    // Placeholder for fight resolve
    // Should be reassigned with Promise.resolve()
    duel.resolve = function (winner) {
        console.log(`Winner: ${winner}`);
    };

    return duel;
}

function getFirstPlayer(healthMax, pressedKeys) {
    return new Player(
        healthMax,
        document.getElementById('left-fighter-indicator'),
        new Action(
            pressedKeys,
            controls.PlayerOneAttack,
            controls.PlayerOneBlock,
            controls.PlayerOneCriticalHitCombination,
            critTimeout,
        ),
    );
}

function getSecondPlayer(healthMax, pressedKeys) {
    return new Player(
        healthMax,
        document.getElementById('right-fighter-indicator'),
        new Action(
            pressedKeys,
            controls.PlayerTwoAttack,
            controls.PlayerTwoBlock,
            controls.PlayerTwoCriticalHitCombination,
            critTimeout,
        ),
    );
}

class Player {
    constructor(healthMax, indicator, action) {
        this.healthMax = healthMax;
        this.indicator = indicator;
        this.action = action;
        this.damage = 0;
    }
    damaged(damage) {
        // To ensure proper usage and not to decrease accumulated damage with this function
        if (damage < 0) {
            return false;
        }
        this.damage = Math.min(this.damage + damage, this.healthMax);
        this.updateIndicator();
    }
    updateIndicator() {
        this.indicator.style.width = `${((this.healthMax - this.damage) / this.healthMax) * 100}%`;
    }
    terminated(damage) {
        return this.damage == this.healthMax;
    }
}

class Action {
    constructor(pressedKeys, attackKey, blockKey, critKeys, critTimeout) {
        this.pressedKeys = pressedKeys;
        this.attackKey = attackKey;
        this.blockKey = blockKey;
        this.critKeys = critKeys;
        this.critLast = 0;
        this.critTimeout = critTimeout;
    }
    isBlock() {
        return this.pressedKeys.has(this.blockKey);
    }
    isAttack() {
        if (!this.isBlock()) {
            return this.pressedKeys.has(this.attackKey);
        }
    }
    isCrit() {
        if (!this.isBlock()) {
            if (Date.now() - this.critLast > this.critTimeout) {
                if (
                    this.critKeys.reduce(
                        (combination, key) => combination && this.pressedKeys.has(key),
                        true,
                    )
                ) {
                    this.critLast = Date.now();
                    return true;
                }
            }
        }
    }
}
